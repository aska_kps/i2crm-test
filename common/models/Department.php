<?php

namespace common\models;

use phpDocumentor\Reflection\Types\Self_;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $title
 * @property int $created_at
 * @property int|null $updated_at
 *
 * @property UserDepartment[] $userDepartments
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    public function behaviors()
    {
        return [
          TimestampBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getDepartmentsToSelect()
    {
        return ArrayHelper::map(self::find()->all(),'id','title');
    }

    /**
     * Gets query for [[UserDepartments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentUsers()
    {
        return $this->hasMany(UserDepartment::class, ['department_id' => 'id']);
    }
}

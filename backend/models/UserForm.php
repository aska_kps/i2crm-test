<?php

namespace backend\models;

use common\models\UserDepartment;
use yii\base\Model;
use common\models\User;
use yii\helpers\ArrayHelper;

class UserForm extends Model
{
    private $user;
    public $name;
    public $email;
    public $username;
    public $password;
    public $status;
    public $departments = [];

    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => User::STATUS_INACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_INACTIVE, User::STATUS_DELETED]],
            [['name', 'username', 'password'], 'string'],
            [['password'], 'safe'],
            [['name', 'username', 'email', 'departments'], 'required'],
            [['email'], 'email']
        ];
    }

    public function setDefaults()
    {
        $user = $this->user;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->name = $user->name;
        $this->status = $user->status;
        $this->departments = ArrayHelper::map($user->getDepartments()->all(), 'id', 'id');
    }


    private function prepareUser()
    {
        $user = $this->user;
        $user->name = $this->name;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;

        if ($this->password) {
            $user->generateEmailVerificationToken();
            $user->setPassword($this->password);
            $user->generateAuthKey();
        }
    }

    public function getUserId()
    {
        return $this->user->id;
    }

    public function save()
    {
        $this->prepareUser();
        if ($this->user->save()) {
            if ($this->user->getUserRole(\Yii::$app->authManager) != User::ROLE_ADMIN) {
                UserDepartment::deleteAll(['user_id' => $this->getUserId()]);
                foreach($this->departments as $departmentId) {
                    $userDepartment = new UserDepartment();
                    $userDepartment->user_id = $this->getUserId();
                    $userDepartment->department_id = $departmentId;
                    $userDepartment->save();
                }
            } else {
                trigger_error('Admin cannot be added to department');
            }
            return true;
        }
        return false;
    }

}
<?php

namespace backend\controllers;

use common\models\Department;
use common\models\User;
use common\models\UserDepartment;
use common\models\UserDepartmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserDepartmentController implements the CRUD actions for UserDepartment model.
 */
class UserDepartmentController extends BaseController
{
    /**
     * Lists all UserDepartment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserDepartmentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $departments = Department::getDepartmentsToSelect();
        $users = User::getUsersToSelect();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'departments' => $departments,
            'users' => $users
        ]);
    }

    /**
     * Displays a single UserDepartment model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserDepartment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserDepartment();
        $departments = Department::getDepartmentsToSelect();
        $users = User::getUsersToSelect();



        if ($this->request->isPost) {
            $userId = $this->request->post()['UserDepartment']['user_id'];
            $user = User::findOne(['id' => $userId]);

            if ($user->getUserRole(\Yii::$app->authManager) != User::ROLE_ADMIN) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                trigger_error('Admin cannot be added to department');
            }


        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'departments' => $departments,
            'users' => $users
        ]);
    }

    /**
     * Updates an existing UserDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $departments = Department::getDepartmentsToSelect();
        $users = User::getUsersToSelect();


        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'departments' => $departments,
            'users' => $users
        ]);
    }

    /**
     * Deletes an existing UserDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserDepartment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return UserDepartment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserDepartment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

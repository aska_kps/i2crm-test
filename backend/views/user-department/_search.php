<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserDepartmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-department-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<!--    --><?//= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id')->dropDownList($users,['prompt' => 'Все'])->label('Select User') ?>

    <?= $form->field($model, 'department_id')->dropDownList($departments,['prompt' => 'Все'])->label('Select Department') ?>

<!--    --><?//= $form->field($model, 'created_at') ?>

<!--    --><?//= $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset','/admin/user-department',['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

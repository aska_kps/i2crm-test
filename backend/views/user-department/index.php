<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserDepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Departments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-department-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Department', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo $this->render('_search', [
        'model' => $searchModel,
        'departments' => $departments,
        'users' => $users
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($searchModel) {
                    $user_name = \common\models\User::findOne(['id' => $searchModel->user_id])->name;
                    return $user_name;
                },
                'label' => 'User Name'
            ],
            [
                'attribute' => 'department_id',
                'value' => function ($searchModel) {
                    $department_title = \common\models\Department::findOne(['id' => $searchModel->department_id])->title;
                    return $department_title;
                },
                'label' => 'Department'
            ],
//            'created_at',
//            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

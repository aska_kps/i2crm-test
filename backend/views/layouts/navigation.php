<nav class="col-md-3 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="/admin/user">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                    Users
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="/admin/department">
                    <img src="/admin/img/d.png">
                    Departments <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="/admin/user-department">
                    <img src="/admin/img/ud.png" alt="Company" title="Company" class="loaded" width="256" height="256">
                    User - Department <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
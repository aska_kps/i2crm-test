<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model \backend\models\UserForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'departments')->widget(\kartik\select2\Select2::class, [
        'name' => 'recommend',
        'data' => $departments,

        'options' => [
            'placeholder' => Yii::t('app', 'Выберите рекомендуемые товары'),
            'multiple' => true,
            'value' =>  $model->departments
        ],
    ]) ?>


    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\User::getStatusesToSelect()) ?>

    <?= $form->field($model, 'password')->textInput(['type' => 'password']) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

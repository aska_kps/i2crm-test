<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\User;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;


        // Creating admin role
        $admin = $auth->createRole('admin');
        $auth->add($admin);


        //Creating admin user
        $user = User::find()->where(['username' => 'admin'])->one();
        if($user) {
            $userId = $user->id;
        } else {
            $userId = $this->createAdmin()->getPrimaryKey();
        }

        $auth->assign($admin, $userId);
    }

    private function createAdmin():User
    {
        $user = new User;
        $user->name = 'admin';
        $user->username = 'admin';
        $user->email = 'test@m.ru';
        $user->setPassword('123456');
        $user->auth_key = \Yii::$app->security->generateRandomString();
        $user->status = User::STATUS_ACTIVE;
        $user->save();

        return $user;
    }



}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_department}}`.
 */
class m211110_090922_create_user_department_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_department}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'department_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->null()
        ]);

        $this->createIndex('idx_unique_user_id_department_id','user_department',['user_id','department_id'],true);

        $this->addForeignKey('fk-department','user_department','department_id','department','id','CASCADE','CASCADE');
        $this->addForeignKey('fk-user','user_department','user_id','user','id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_department}}');
    }
}
